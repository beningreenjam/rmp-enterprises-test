export default {
    duration: [
        'Insight (< 4 Weeks)',
        'Internship (1-4 Months)',
        'Placement (5-9 Months)',
        'Placement (10+ Months)',
    ],
    location: [
        'London',
        'Greater London',
        'South East',
        'South West',
        'East Anglia',
        'West Midlands',
        'East Midlands',
        'North East',
        'North West',
        'Yorkshire Humberside',
        'Northern Ireland',
        'Republic of Ireland',
        'Europe',
        'Channel Islands',
        'China',
        'Rest of the World',
        'USA',
        'Scotland',
        'Wales',
    ]
};