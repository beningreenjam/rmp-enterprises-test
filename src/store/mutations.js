export default {
    initResults(state, results) {
        state.results = results;
    },

    addArrayID(state, {key, id}) {
        if(!~state[key].indexOf(id)) {
            state[key].push(id);

            localStorage.setItem(key, JSON.stringify(state[key]));
        }
    },

    removeArrayID(state, {key, index}) {
        if(typeof state[key][index] != 'undefined') {
            state[key].splice(index, 1);

            localStorage.setItem(key, JSON.stringify(state[key]));
        }
    },

    addNotification(state, data) {
        state.notifications.push(data);
    },

    removeNotification(state, id) {
        state.notifications.splice(state.notifications.indexOf(id), 1);
    },

    toggleSidePanel(state, showing) {
        state.showingSidePanel = !showing;
    },

    setFilter(state, filter) {
        const {type, name} = filter;
        const index = state.filters[type].indexOf(name);

        if(!!~index) {
            state.filters[type].splice(index, 1);
        }
        else {
            state.filters[type].push(name);
        }
    },

    clearFilters(state) {
        state.filters = {
            duration: [],
            location: [],
        }
    },
}