export default {
    initResults({commit}, results) {
        commit('initResults', results)
    },

    addShortlistID({commit}, {id, title, company_name, company_id}) {
        const key = 'shortlistIDs';

        commit('addArrayID', {key, id});
        commit('addNotification', {
            key,
            action: 'added',
            data: {
                id,
                title,
                company_id,
                company_name
            }
        });
    },

    removeShortlistID({commit}, {id, index, title, company_name}) {
        const key = 'shortlistIDs';

        commit('removeArrayID', {key, index});
        commit('addNotification', {
            key,
            action: 'removed',
            data: {
                id,
                title,
                company_name
            }
        });
    },

    addFollowingCompanyID({commit}, {company_name, company_id}) {
        const key = 'followingCompanyIDs';

        commit('addArrayID', {key, id: company_id});
        commit('addNotification', {
            key,
            action: 'added',
            data: {
                company_id,
                company_name
            }
        });
    },

    removeNotification({commit}, id) {
        commit('removeNotification', id);
    },

    toggleSidePanel({commit}, showing) {
        commit('toggleSidePanel', showing);
    },

    setFilter({commit}, filter) {
        commit('setFilter', filter)
    },

    clearFilters({commit}) {
        commit('clearFilters')
    },
}