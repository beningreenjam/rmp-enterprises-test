const shortlistIDs = () => {
    const shortlistIDs = localStorage.getItem('shortlistIDs');
    return shortlistIDs ? JSON.parse(shortlistIDs) : []
};

const followingCompanyIDs = () => {
    const followingCompanyIDs = localStorage.getItem('followingCompanyIDs');
    return followingCompanyIDs ? JSON.parse(followingCompanyIDs) : []
};

export default {
    results: [],
    shortlistIDs: shortlistIDs(),
    followingCompanyIDs: followingCompanyIDs(),
    notifications: [],
    showingSidePanel: false,
    filters: {
        duration: [],
        location: [],
    },
};