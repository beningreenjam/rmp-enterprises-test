// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './app';
import store from './store';

Vue.config.productionTip = false;
Vue.config.devtools = true;

new Vue({
    el: '#app',
    store,
    components: {App},
    template: '<App/>'
});