export default {
    computed: {
        showingSidePanel: (vm) => {
            return vm.$store.state.showingSidePanel;
        }
    }
};